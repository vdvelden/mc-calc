package au.net.vdvelden.mccalc;

import au.net.vdvelden.mccalc.client.CalculatorConfiguration;
import au.net.vdvelden.mccalc.client.Items.ItemRenderRegister;
import au.net.vdvelden.mccalc.client.Items.ModItems;
import au.net.vdvelden.mccalc.client.KeyBindings;
import au.net.vdvelden.mccalc.client.KeyInputHandler;
import au.net.vdvelden.mccalc.common.proxies.ClientProxy;
import au.net.vdvelden.mccalc.common.proxies.CommonProxy;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.lwjgl.input.Keyboard;

/**
 * Created by Lachlan on 6/07/2015.
 */
@Mod(modid = Main.MODID, name = Main.MODNAME, version = Main.MODVERSION, guiFactory = "au.net.vdvelden.mccalc.client.gui.CalculatorGuiFactory")
public class Main {
    public static final String MODID = "mccalc";
    public static final String MODNAME = "Minecraft Calculator";
    public static final String MODVERSION = "1.01";

    @SidedProxy(clientSide = "au.net.vdvelden.mccalc.common.proxies.ClientProxy", serverSide = "au.net.vdvelden.mccalc.common.proxies.CommonProxy", modId = MODID)
    public static CommonProxy proxy;
    public static ClientProxy clientProxy;

    public static Configuration config;

    @Mod.Instance(MODID)
    public static Main instance;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){

        config = new Configuration(event.getSuggestedConfigurationFile());
        CalculatorConfiguration.getConfig();

        if(CalculatorConfiguration.EnableCalculatorItem) {
            ModItems.createItems();
        }else{
            KeyBindings.calcOpen = new KeyBinding("key.calcOpen", Keyboard.KEY_V, "key.categories.mccalc");
            ClientRegistry.registerKeyBinding(KeyBindings.calcOpen);
            FMLCommonHandler.instance().bus().register(new KeyInputHandler());
        }

    }
    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event){
        if(event.modID.equals(MODID)){
            CalculatorConfiguration.getConfig();
        }
    }
    @Mod.EventHandler
    public void Init(FMLInitializationEvent event){

        if(CalculatorConfiguration.EnableCalculatorItem) {
            ItemRenderRegister.registerItemRenderer();
        }
    }
}
