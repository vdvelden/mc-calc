package au.net.vdvelden.mccalc.client;

import au.net.vdvelden.mccalc.Main;
import net.minecraftforge.fml.common.FMLCommonHandler;

/**
 * Created by Lachlan on 21/09/2015.
 */
public class CalculatorConfiguration {

    public static boolean EnableCalculatorItem;
    public static final String EnableCalculatorItem_Name = "Enable Calculator Item";
    public static final boolean EnableCalculatorItem_Default = false;

    public static void getConfig(){
        FMLCommonHandler.instance().bus().register(Main.instance);

        final String Recipes = Main.config.CATEGORY_GENERAL + Main.config.CATEGORY_SPLITTER + "Recipes";
        Main.config.addCustomCategoryComment(Recipes, "Enable or disable Recipes");
        EnableCalculatorItem = Main.config.get(Recipes, EnableCalculatorItem_Name, EnableCalculatorItem_Default).getBoolean();
        if(Main.config.hasChanged()){
            Main.config.save();
        }
    }
}
