package au.net.vdvelden.mccalc.client.Items;

import au.net.vdvelden.mccalc.Main;
import au.net.vdvelden.mccalc.client.Items.ItemCalculator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

/**
 * Created by Lachlan on 21/09/2015.
 */
public final class ItemRenderRegister {

    public static void registerItemRenderer() {
        registerItem(ModItems.CalculatorItem);
        registerItem(ModItems.AdvancedCalculatorItem);
    }


    public static void registerItem(Item item) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
                .register(item, 0, new ModelResourceLocation(Main.MODID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }
}
