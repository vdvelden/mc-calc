package au.net.vdvelden.mccalc.client.Items;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import net.minecraft.client.Minecraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

/**
 * Created by Lachlan on 21/09/2015.
 */
public class ItemAdvancedCalculator extends Item {
    public static final String UNLOCALIZEDNAME = "Item_AdvancedCalculator";

    Minecraft mc = Minecraft.getMinecraft();

    public ItemAdvancedCalculator() {
        super();
        this.setUnlocalizedName(UNLOCALIZEDNAME);
        this.setCreativeTab(CreativeTabs.tabTools);
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn) {
            mc.displayGuiScreen(new GuiCalculator(true));
        return super.onItemRightClick(itemStackIn, worldIn, playerIn);
    }
}
