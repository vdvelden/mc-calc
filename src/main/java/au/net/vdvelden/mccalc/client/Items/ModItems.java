package au.net.vdvelden.mccalc.client.Items;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

/**
 * Created by Lachlan on 21/09/2015.
 */
public final class ModItems {

    public static Item CalculatorItem;
    public static Item AdvancedCalculatorItem;
    public static void createItems() {
        GameRegistry.registerItem(CalculatorItem = new ItemCalculator(), ItemCalculator.UNLOCALIZEDNAME );
        GameRegistry.registerItem(AdvancedCalculatorItem = new ItemAdvancedCalculator(), ItemAdvancedCalculator.UNLOCALIZEDNAME );
    }
}
