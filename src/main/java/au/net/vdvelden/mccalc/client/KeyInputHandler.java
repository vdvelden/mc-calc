package au.net.vdvelden.mccalc.client;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent;

/**
 * Created by Lachlan on 7/07/2015.
 */
public class KeyInputHandler {

    @SubscribeEvent
    public void onKeyInput(InputEvent.KeyInputEvent event) {
        if(KeyBindings.calcOpen.isPressed()){
            Minecraft mc = Minecraft.getMinecraft();
            mc.displayGuiScreen(new GuiCalculator());
        }
    }

}