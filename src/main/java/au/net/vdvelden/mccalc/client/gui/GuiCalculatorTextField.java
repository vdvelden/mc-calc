package au.net.vdvelden.mccalc.client.gui;

import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiTextField;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class GuiCalculatorTextField extends GuiTextField {

    public GuiCalculatorTextField(int controlID, FontRenderer renderer, int x, int y, int width, int height)
    {
        super(controlID, renderer, x, y, width, height);
    }
}
