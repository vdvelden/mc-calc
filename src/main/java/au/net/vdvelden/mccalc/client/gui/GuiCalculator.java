package au.net.vdvelden.mccalc.client.gui;

import au.net.vdvelden.mccalc.client.CalculatorConfiguration;
import au.net.vdvelden.mccalc.common.Calculator.layout.AdvancedCalculatorLayout;
import au.net.vdvelden.mccalc.common.Calculator.layout.BasicCalculatorLayout;
import au.net.vdvelden.mccalc.common.Calculator.layout.CalculatorLayout;
import au.net.vdvelden.mccalc.common.Calculator.process.CalculatorAutoComplete;
import au.net.vdvelden.mccalc.common.Calculator.result.CalculatorResultSuccess;
import au.net.vdvelden.mccalc.common.Calculator.result.ICalculatorResult;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorVariables;
import au.net.vdvelden.mccalc.common.Utilities;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import java.io.IOException;

/**
 * Created by Lachlan on 6/07/2015.
 */
public class GuiCalculator extends GuiScreen {
    private final Minecraft mc;
    private int currentButtonID = 0;

    public boolean IsAdvanced;
    public CalculatorLayout layout;

    public boolean removeTextAfterCalculate = true;

    public GuiCalculator() {
        mc = Minecraft.getMinecraft();
    }
    public GuiCalculator(boolean advanced){
        mc = Minecraft.getMinecraft();
        IsAdvanced = advanced;

    }
    public void initGui() {

        if(!CalculatorConfiguration.EnableCalculatorItem) {
            IsAdvanced = GuiScreen.isShiftKeyDown();
        }

        if(IsAdvanced)
            layout = new AdvancedCalculatorLayout(this);
        else
            layout = new BasicCalculatorLayout(this);
        //Place Done button on bottom left of screen with 10 padding
        this.buttonList.add(new GuiButton(0, this.width - 75 - 10, this.height - 20 - 10, 75, 20, "Done"));
        currentButtonID++;
    }
    public void addResult(ICalculatorResult result){
        layout.results.add(result);
    }



    protected void keyTyped(char typedChar, int keyCode) throws IOException
    {
        if(layout.keyTyped(typedChar, keyCode)){
            super.keyTyped(typedChar, keyCode);
        }
        if(keyCode == 1){
            mc.displayGuiScreen(null);
        }
    }

    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        layout.mouseClicked(mouseX, mouseY, mouseButton);
        super.mouseClicked(mouseX, mouseY, mouseButton);
    }

    public void actionPerformed(GuiButton button) throws IOException
    {
        if (button.enabled) {
            if (button.id == 0) {
                mc.displayGuiScreen(null);
            }
        }
    }

    public void drawScreen(int mouseX, int mouseY, float partialTicks)
    {
        drawWorldBackground(20);
        layout.drawLayout(mouseX, mouseY, partialTicks);
        super.drawScreen(mouseX, mouseY, partialTicks);
    }
}
