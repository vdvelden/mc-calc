package au.net.vdvelden.mccalc.client.gui;

import au.net.vdvelden.mccalc.Main;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.client.config.GuiConfig;

/**
 * Created by Lachlan on 21/09/2015.
 */
public class ConfigGui extends GuiConfig {
    public ConfigGui(GuiScreen screen){
        super(screen, new ConfigElement(Main.config.getCategory(Configuration.CATEGORY_GENERAL)).getChildElements(),
                Main.MODNAME, false, false, GuiConfig.getAbridgedConfigPath(Main.config.toString()));
    }
}
