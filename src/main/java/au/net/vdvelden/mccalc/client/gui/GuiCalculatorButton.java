package au.net.vdvelden.mccalc.client.gui;

import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonItem;
import net.minecraft.client.gui.GuiButton;

/**
 * Created by Lachlan on 7/07/2015.
 */
public class GuiCalculatorButton extends GuiButton {

    public final CalculatorButtonItem item;
    public GuiCalculatorButton(int buttonId, int x, int y, int widthIn, int heightIn, CalculatorButtonItem item)
    {
        super(buttonId, x, y, widthIn, heightIn, item.buttonText);
        this.item = item;
    }

}
