package au.net.vdvelden.mccalc.common.Calculator.items;

/**
 * Created by Lachlan on 7/07/2015.
 */
public enum CalculatorButtonType {
    Number,
    Math,
    Action,
    Variable,
    PlaceHolder
}
