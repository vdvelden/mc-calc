package au.net.vdvelden.mccalc.common.Calculator.result;

/**
 * Created by Lachlan on 7/07/2015.
 */
public interface ICalculatorResult {
    public String getString();
}
