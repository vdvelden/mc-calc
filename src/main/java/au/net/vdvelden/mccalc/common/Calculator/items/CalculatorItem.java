package au.net.vdvelden.mccalc.common.Calculator.items;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

/**
 * Created by Lachlan on 7/07/2015.
 */
public abstract class CalculatorItem {

    public int width = 20;
    public final int height = 20;

    public int rowSpan = 1;
    public int colSpan = 1;
    public int ID = -1;

    public abstract void drawItem(Minecraft mc, int mouseX, int mouseY, float partialTicks);

    public abstract void actionPerformed(GuiButton button);

    public abstract void initItem(CalculatorItemPosition position);

}
