package au.net.vdvelden.mccalc.common.Calculator.values;

import net.minecraft.client.Minecraft;

/**
 * Created by Lachlan on 8/07/2015.
 */
public class CalculatorVariables {
    public static final char PLAYER = 'P';
    public static final char BLOCK_SELECTED = 'B';

    public static final char X = 'x';
    public static final char Y = 'y';
    public static final char Z = 'z';

    public static char[] getAllVariableObjectChars(){
        char[] array = new char[2];
        array[0] = PLAYER;
        array[1] = BLOCK_SELECTED;
        return array;
    }
    public static char[] getAllVariableValueChars(){
        char[] array = new char[3];
        array[0] = X;
        array[1] = Y;
        array[2] = Z;
        return array;
    }
    public static String getVariableTag(char object, char value){
        return "{" + object + value + "}";
    }

    public static double getVariableValue(char object, char value){
        Minecraft mc = Minecraft.getMinecraft();
        switch (object){
            case (PLAYER):
                switch (value){
                    case(X):
                        return mc.thePlayer.posX;
                    case(Y):
                        return mc.thePlayer.posY;
                    case(Z):
                        return mc.thePlayer.posZ;
                }
            case (BLOCK_SELECTED):
                switch (value){
                    case(X):
                        return mc.objectMouseOver.getBlockPos().getX();
                    case(Y):
                        return mc.objectMouseOver.getBlockPos().getY();
                    case(Z):
                        return mc.objectMouseOver.getBlockPos().getZ();
                }
            default:
                return 0.0D;
        }
    }
    public static String findAndReplace(String value){
        for(char varObject : getAllVariableObjectChars()){
            for (char varValue : getAllVariableValueChars()){
                value = value.replace(getVariableTag(varObject, varValue), "(" + String.valueOf(getVariableValue(varObject, varValue)) + ")");
            }
        }
        return value;
    }
}
