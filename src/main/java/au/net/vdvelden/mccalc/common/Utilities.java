package au.net.vdvelden.mccalc.common;

import au.net.vdvelden.mccalc.common.Calculator.process.CalculatorBaseVisitorImpl;
import au.net.vdvelden.mccalc.common.Calculator.process.CalculatorLexer;
import au.net.vdvelden.mccalc.common.Calculator.process.CalculatorParser;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorVariables;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by Lachlan on 7/07/2015.
 */
public class Utilities {
    public static final String legalKeys = "1234567890-+/*.{}^()e";

    public static double calculate(String text) {
        return calculate(text, false);
    }
    public static double calculate(String text, boolean advanced) {
        if(advanced) {
            text = CalculatorVariables.findAndReplace(text);
        }else{
            if(text != CalculatorVariables.findAndReplace(text)){
                //Calculator is not advanced, but a variable has been entered.
                return 0.0D;
            }
        }
        double result = 0.0D;
        try {
            ANTLRInputStream input = new ANTLRInputStream(text);
            CalculatorLexer lexer = new CalculatorLexer(input);
            CommonTokenStream tokens = new CommonTokenStream(lexer);
            CalculatorParser parser = new CalculatorParser(tokens);
            ParseTree tree = parser.input();

            CalculatorBaseVisitorImpl calcVisitor = new CalculatorBaseVisitorImpl();
            result = calcVisitor.visit(tree);
        } catch (Exception ex) {}
        return result;
    }
    public static boolean isLegalMathKey(char typedChar) {
        return legalKeys.contains(String.valueOf(typedChar));
    }
    public static double roundDouble(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}
