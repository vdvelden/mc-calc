package au.net.vdvelden.mccalc.common.Calculator.items;

import au.net.vdvelden.mccalc.client.gui.GuiCalculatorButton;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorButtonAction;
import au.net.vdvelden.mccalc.common.Calculator.layout.CalculatorLayout;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class CalculatorButtonItem extends CalculatorItem {

    public GuiCalculatorButton button;
    public CalculatorButtonType type;
    public CalculatorButtonAction action;
    public String buttonText;
    public int controlId = -1;

    private CalculatorLayout layout;
    private CalculatorItemPosition position;

    public CalculatorButtonItem(CalculatorLayout layout, String text) {
        this(layout, text, CalculatorButtonType.Number);
    }
    public CalculatorButtonItem(CalculatorLayout layout, String value, CalculatorButtonType type){
        this(layout, value, type, 1, 1);
    }
    public CalculatorButtonItem(CalculatorLayout layout, String value, CalculatorButtonType type, int colspan, int rowspan){
        this(layout, value, type, CalculatorButtonAction.None, colspan, rowspan);
    }
    public CalculatorButtonItem(CalculatorLayout layout, String value, CalculatorButtonType type, CalculatorButtonAction action){
        this(layout, value, type, action, 1, 1);
    }
    public CalculatorButtonItem(CalculatorLayout layout, String value, CalculatorButtonType type, CalculatorButtonAction action, int colspan, int rowspan){
        this.type = type;
        this.colSpan = colspan;
        this.rowSpan = rowspan;
        this.action = action;
        this.buttonText = value;
        this.layout = layout;
        this.position = new CalculatorItemPosition(-1, -1);
        controlId = layout.nextControlID();
    }
    public void initItem(CalculatorItemPosition position){
        this.position = position;
        setButton();
    }
    public void setButton(){
        if(!position.IsValid())
            layout.getPositionOf(this);
        button = new GuiCalculatorButton(
                controlId,
                layout.layoutX + (position.X * layout.colWidth) + (position.X * layout.padding), //x
                layout.layoutY + (position.Y * layout.rowHeight) + (position.Y * layout.padding), //y
                layout.colWidth * this.colSpan + (layout.padding * (this.colSpan - 1)), //width
                layout.rowHeight * this.rowSpan + (layout.padding * (this.rowSpan - 1)), //height
                this); //value
    }
    @Override
    public void drawItem(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        if(!position.IsValid()){
            setButton();
        }
        button.drawButton(mc, mouseX, mouseY);
    }

    @Override
    public void actionPerformed(GuiButton button) {

    }
}
