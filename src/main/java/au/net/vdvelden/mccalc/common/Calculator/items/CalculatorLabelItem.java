package au.net.vdvelden.mccalc.common.Calculator.items;


import au.net.vdvelden.mccalc.common.Calculator.layout.CalculatorLayout;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class CalculatorLabelItem extends CalculatorItem {

    public String Text;

    private CalculatorLayout layout;
    private CalculatorItemPosition position;

    public CalculatorLabelItem(CalculatorLayout layout, String text) {
        this(layout, text, 1, 1);
    }

    public CalculatorLabelItem(CalculatorLayout layout, String value, int colspan, int rowspan){
        this.colSpan = colspan;
        this.rowSpan = rowspan;
        this.Text = value;
        this.layout = layout;
        this.position = new CalculatorItemPosition(-1, -1);
    }

    public void initItem(CalculatorItemPosition position){
        this.position = position;
    }

    @Override
    public void drawItem(Minecraft mc, int mouseX, int mouseY, float partialTicks) {
        int textWidth = mc.fontRendererObj.getStringWidth(Text);

        int x = layout.layoutX + (position.X * layout.colWidth) + (position.X * layout.padding);
        int y = layout.layoutY + (position.Y * layout.rowHeight) + (position.Y * layout.padding);
        int width = layout.colWidth * this.colSpan + (layout.padding * (this.colSpan - 1));
        if(textWidth < width){
            x += (width / 2) - (textWidth / 2);
            y += (height / 2) - (mc.fontRendererObj.FONT_HEIGHT / 2);
        }
        mc.fontRendererObj.drawSplitString(Text, x, y, width,  14737632);
    }

    @Override
    public void actionPerformed(GuiButton button) {

    }
}
