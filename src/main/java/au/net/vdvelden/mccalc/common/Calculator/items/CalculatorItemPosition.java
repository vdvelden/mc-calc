package au.net.vdvelden.mccalc.common.Calculator.items;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class CalculatorItemPosition {
    public int X;
    public int Y;

    public CalculatorItemPosition(int x, int y){
        X = x;
        Y = y;
    }
    public boolean IsValid(){
        return X >= 0 && Y >= 0;
    }
}
