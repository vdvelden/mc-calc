package au.net.vdvelden.mccalc.common.Calculator.result;

import au.net.vdvelden.mccalc.common.Utilities;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class CalculatorResultSuccess implements ICalculatorResult {
    public double Result;
    public String Equation;
    public CalculatorResultSuccess(String equation){
        this(equation, Utilities.calculate(equation));
    }
    public CalculatorResultSuccess(String equation, double result){
        Result = result;
        Equation = equation;
    }

    public String getString() {
        return Equation + " = " + String.valueOf(Utilities.roundDouble(Result, 2));
    }
}
