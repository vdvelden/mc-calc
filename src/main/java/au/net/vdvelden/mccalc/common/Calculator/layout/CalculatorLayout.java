package au.net.vdvelden.mccalc.common.Calculator.layout;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import au.net.vdvelden.mccalc.client.gui.GuiCalculatorButton;
import au.net.vdvelden.mccalc.client.gui.GuiCalculatorTextField;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonItem;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorItem;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorItemPosition;
import au.net.vdvelden.mccalc.common.Calculator.process.CalculatorAutoComplete;
import au.net.vdvelden.mccalc.common.Calculator.result.CalculatorResultSuccess;
import au.net.vdvelden.mccalc.common.Calculator.result.ICalculatorResult;
import au.net.vdvelden.mccalc.common.Utilities;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.ChatComponentText;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lachlan on 9/07/2015.
 */
public abstract class CalculatorLayout {

    public GuiCalculator screen;

    public int padding = 5;
    public int width = 6;
    public int height = 6;

    public int colWidth = 20;
    public int rowHeight = 20;

    public int layoutX = 0;
    public int layoutY = 0;

    public int currentControlId = 2;

    public GuiCalculatorTextField textField;
    public CalculatorItem[][] calculatorItems;
    public List<ICalculatorResult> results;
    private CalculatorAutoComplete calculatorAutoComplete;
    protected Minecraft mc;

    public abstract void initCalculatorItems();

    public CalculatorLayout(GuiCalculator screen){
        this(screen, -1, -1, -1, -1);
    }
    public CalculatorLayout(GuiCalculator screen, int width, int height){
        this(screen, -1, -1, width, height);
    }
    public CalculatorLayout(GuiCalculator screen, int x, int y, int width, int height){
        mc = Minecraft.getMinecraft();
        this.screen = screen;
        init(x, y, width, height);
        initCalculatorItems();
        for(int posX=0; posX < this.width; posX++) {
            for (int posY = 0; posY < this.height; posY++) {
                if(calculatorItems[posY][posX] != null) {
                    calculatorItems[posY][posX].initItem(new CalculatorItemPosition(posX, posY));
                }
            }
        }
        results = new ArrayList<ICalculatorResult>();
        calculatorAutoComplete = new CalculatorAutoComplete(screen);
    }
    private void init(int x, int y, int width, int height){
        layoutX = x;
        layoutY = y;
        if(width != -1 && height != -1){
            this.width = width;
            this.height = height;
        }
        if(x == -1 && y == -1) {
            this.layoutX = (screen.width / 2) - (getLayoutWidth() / 2);
            this.layoutY = (screen.height / 2) - (getLayoutHeight() / 2);
        }
        textField = new GuiCalculatorTextField(1, mc.fontRendererObj, this.layoutX, this.layoutY, getLayoutWidth(), 20);
        textField.setFocused(true);
        textField.setCanLoseFocus(false);
        textField.setMaxStringLength(1000);

        this.layoutY += 20 + padding;
    }
    protected void setSize(int width, int height){
        init(layoutX, layoutY, width, height);
    }
    protected void setWidth(int width){
        init(layoutX, layoutY, width, height);
    }
    protected void setHeight(int height){
        init(layoutX, layoutY, width, height);
    }
    protected void setX(int layoutX){
        init(layoutX, layoutY, width, height);
    }
    protected void setY(int layoutY){
        init(layoutX, layoutY, width, height);
    }

    public boolean keyTyped(char typedChar, int keyCode)
    {
        if (keyCode == 28) {
            processCalculate(GuiScreen.isShiftKeyDown(), false, screen.IsAdvanced);
        }else if (keyCode == 15) {
            if(results.size() > 0) {
                ICalculatorResult lastResult = results.get(results.size() - 1);
                if (lastResult instanceof CalculatorResultSuccess)
                    textField.setText(textField.getText() + Utilities.roundDouble(((CalculatorResultSuccess) lastResult).Result, 2));
            }
        }else if(Utilities.isLegalMathKey(typedChar) || keyCode == 14) {
            this.textField.textboxKeyTyped(typedChar, keyCode);
            return true;
        }else{
            calculatorAutoComplete.processAutoComplete(typedChar);
        }
        return false;
    }
    public void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        if (mouseButton == 0)
        {
            for(int posX=0; posX < width; posX++) {
                for (int posY = 0; posY < height; posY++) {
                    if(calculatorItems[posY][posX] != null) {

                        if(calculatorItems[posY][posX] instanceof CalculatorButtonItem) {
                            CalculatorButtonItem item = (CalculatorButtonItem) calculatorItems[posY][posX];
                            if (item.button.mousePressed(this.mc, mouseX, mouseY)) {
                                this.actionPerformed(item.button);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    public void actionPerformed(GuiButton button) throws IOException {
        if (button.enabled) {
            if (button instanceof GuiCalculatorButton) {
                GuiCalculatorButton buttonCalculator = (GuiCalculatorButton) button;
                switch (buttonCalculator.item.type) {
                    case Number:
                        this.textField.setText(this.textField.getText() + buttonCalculator.item.buttonText);
                        break;
                    case Math:
                        if(buttonCalculator.item.buttonText == "Sqrt")
                            this.textField.setText(this.textField.getText() + "^(1/2)");
                        else
                            this.textField.setText(this.textField.getText() + buttonCalculator.item.buttonText);
                        break;
                    case Action:
                        switch (buttonCalculator.item.action) {
                            case RemoveAll:
                                this.textField.setText("");
                                break;
                            case RemoveLast:
                                if (this.textField.getText().length() > 0)
                                    this.textField.setText(this.textField.getText().substring(0, this.textField.getText().length() - 1));
                                break;
                            case Calculate:
                                processCalculate(GuiScreen.isShiftKeyDown(), false, screen.IsAdvanced);
                                break;
                            case CopyToClipBoard:
                                processCalculate(GuiScreen.isShiftKeyDown(), true, screen.IsAdvanced);
                        }
                        break;
                    case Variable:
                        this.textField.setText(this.textField.getText() + "{" + buttonCalculator.item.buttonText + "}");
                        break;
                }

            }
        }
    }
    public void processCalculate(){
        this.processCalculate(false, false, false);
    }
    public void processCalculate(boolean ShiftEnter){
        this.processCalculate(ShiftEnter, false, false);
    }
    //ShiftEnter: Closes calculator and then displays result in chat.
    public void processCalculate(boolean ShiftEnter, boolean copy, boolean isAdvanced){
        if(this.textField.getText().length() > 1) {
            double value =  Utilities.calculate(this.textField.getText(), isAdvanced);
            CalculatorResultSuccess result = new CalculatorResultSuccess(this.textField.getText(), value);
            if(copy){
                StringSelection stringSelection = new StringSelection(String.valueOf(Utilities.roundDouble(result.Result, 2)));
                Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard ();
                clipboard.setContents(stringSelection, null);
            }
            if (ShiftEnter) {
                mc.thePlayer.addChatComponentMessage(new ChatComponentText(result.getString()));
                mc.displayGuiScreen(null);
            } else {
                results.add(result);
                if(true)
                    this.textField.setText("");
            }
        }else{
            //results.add(new CalculateResultError("No Text Entered."));
        }
    }
    public void drawLayout(int mouseX, int mouseY, float partialTicks){
        for(int posX=0; posX < width; posX++) {
            for (int posY = 0; posY < height; posY++) {
                if(calculatorItems[posY][posX] != null) {
                    calculatorItems[posY][posX].drawItem(mc, mouseX, mouseY, partialTicks);
                }
            }
        }
        textField.drawTextBox();
        int lineHeight = 10;
        int startX = 5;
        int startY = screen.height - (lineHeight + lineHeight * results.size());
        for (ICalculatorResult result : results){
            screen.drawString(mc.fontRendererObj, result.getString(), startX, startY + (results.indexOf(result) * lineHeight), 14737632);
        }
    }
    public int getLayoutWidth(){
        return (width * (padding + colWidth));
    }
    public int getLayoutHeight(){
        return (height * (padding + rowHeight));
    }
    public int nextControlID(){
        return currentControlId++;
    }
    public CalculatorItemPosition getPositionOf(CalculatorItem item){
        for(int posX=0; posX < width; posX++) {
            for (int posY = 0; posY < height; posY++) {
                if(calculatorItems[posY][posX] != null)
                    if(calculatorItems[posY][posX].equals(item))
                        return new CalculatorItemPosition(posX, posY);
            }
        }
        return new CalculatorItemPosition(-1, -1);
    }
}
