package au.net.vdvelden.mccalc.common.proxies;

import au.net.vdvelden.mccalc.client.Items.ItemRenderRegister;

/**
 * Created by Lachlan on 21/09/2015.
 */
public class ClientProxy extends CommonProxy {
    @Override
    public void registerRenders() {
        ItemRenderRegister.registerItemRenderer();
    }
}
