package au.net.vdvelden.mccalc.common.Calculator.process;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import au.net.vdvelden.mccalc.common.Calculator.layout.CalculatorLayout;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorVariables;

/**
 * Created by Lachlan on 21/09/2015.
 */
public class CalculatorAutoComplete {
    private char lastIgnoredChar;
    private GuiCalculator guiCalculator;
    public CalculatorAutoComplete(GuiCalculator gui){
        guiCalculator = gui;
    }

    public void processAutoComplete(char typedChar){
        switch (typedChar){
            case (CalculatorVariables.X):
                switch (lastIgnoredChar){
                    case (CalculatorVariables.PLAYER):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.PLAYER, CalculatorVariables.X));
                        break;
                    case (CalculatorVariables.BLOCK_SELECTED):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.BLOCK_SELECTED, CalculatorVariables.X));
                        break;
                }
                break;
            case (CalculatorVariables.Y):
                switch (lastIgnoredChar){
                    case (CalculatorVariables.PLAYER):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.PLAYER, CalculatorVariables.Y));
                        break;
                    case (CalculatorVariables.BLOCK_SELECTED):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.BLOCK_SELECTED, CalculatorVariables.Y));
                        break;
                }
                break;
            case (CalculatorVariables.Z):
                switch (lastIgnoredChar){
                    case (CalculatorVariables.PLAYER):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.PLAYER, CalculatorVariables.Z));
                        break;
                    case (CalculatorVariables.BLOCK_SELECTED):
                        guiCalculator.layout.textField.setText(guiCalculator.layout.textField.getText() + CalculatorVariables.getVariableTag(CalculatorVariables.BLOCK_SELECTED, CalculatorVariables.Z));
                        break;
                }
                break;
            default:
                lastIgnoredChar = typedChar;
                break;
        }
    }
}
