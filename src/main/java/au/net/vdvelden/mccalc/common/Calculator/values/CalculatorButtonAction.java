package au.net.vdvelden.mccalc.common.Calculator.values;

/**
 * Created by Lachlan on 7/07/2015.
 */
public enum CalculatorButtonAction {
    RemoveAll,
    RemoveLast,
    Calculate,
    PositiveNegative,
    CopyToClipBoard,
    None
}
