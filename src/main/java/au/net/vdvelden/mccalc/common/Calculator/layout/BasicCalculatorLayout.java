package au.net.vdvelden.mccalc.common.Calculator.layout;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorButtonAction;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonItem;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonType;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorItem;
import net.minecraft.client.gui.GuiScreen;

/**
 * Created by Lachlan on 7/07/2015.
 */
public class BasicCalculatorLayout extends CalculatorLayout {
    public BasicCalculatorLayout(GuiCalculator screen){
        super(screen);
    }
    public BasicCalculatorLayout(GuiCalculator screen, int x, int y) {
        super(screen, x, y);
    }

    public void initCalculatorItems() {
        calculatorItems = new CalculatorItem[height][width];
        int x = 0;
        int y = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "<-", CalculatorButtonType.Action, CalculatorButtonAction.RemoveLast, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Clear", CalculatorButtonType.Action, CalculatorButtonAction.RemoveAll, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;//new CalculatorButtonItem(this, "Px", CalculatorButtonType.Variable);
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "7");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "8");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "9");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "/", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;//new CalculatorButtonItem(this, "Py", CalculatorButtonType.Variable);
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "4");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "5");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "6");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "*", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;//new CalculatorButtonItem(this, "Pz", CalculatorButtonType.Variable);
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "1");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "2");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "3");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "-", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "0", CalculatorButtonType.Number, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, ".");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "+", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Calculate", CalculatorButtonType.Action, CalculatorButtonAction.Calculate, 6, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;

    }

}
