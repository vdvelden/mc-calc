package au.net.vdvelden.mccalc.common.Calculator.layout;

import au.net.vdvelden.mccalc.client.gui.GuiCalculator;
import au.net.vdvelden.mccalc.common.Calculator.values.CalculatorButtonAction;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonItem;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorButtonType;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorItem;
import au.net.vdvelden.mccalc.common.Calculator.items.CalculatorLabelItem;
import net.minecraft.client.gui.GuiScreen;

/**
 * Created by Lachlan on 7/07/2015.
 */
public class AdvancedCalculatorLayout extends CalculatorLayout {
    private static final int width = 10;
    private static final int height = 8;
    public AdvancedCalculatorLayout(GuiCalculator screen){
        super(screen, -1, -1, width, height);
    }
    public AdvancedCalculatorLayout(GuiCalculator screen, int x, int y) {
        super(screen, x, y);
    }

    public void initCalculatorItems() {
        calculatorItems = new CalculatorItem[height][width];
        //Each Section is one row
        int x = 0;
        int y = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "(");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, ")");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "7");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "8");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "9");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "/", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Sqrt", CalculatorButtonType.Math, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "4");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "5");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "6");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "*", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "1");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "2");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "3");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "-", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "0", CalculatorButtonType.Number, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, ".");
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "+", CalculatorButtonType.Math);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorLabelItem(this, "X", 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorLabelItem(this, "Y", 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorLabelItem(this, "Z", 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = new CalculatorLabelItem(this, "Player", 2 , 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Px", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Py", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Pz", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = new CalculatorLabelItem(this, "Block Selected", 2 , 1);;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Bx", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "By", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Bz", CalculatorButtonType.Variable, 2, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        y += 1;
        x = 0;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Copy Result", CalculatorButtonType.Action, CalculatorButtonAction.CopyToClipBoard, 3, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = new CalculatorButtonItem(this, "Calculate", CalculatorButtonType.Action, CalculatorButtonAction.Calculate, 7, 1);
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;
        calculatorItems[y][x++] = null;

    }

}
