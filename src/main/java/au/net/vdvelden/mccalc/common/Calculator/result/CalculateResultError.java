package au.net.vdvelden.mccalc.common.Calculator.result;

/**
 * Created by Lachlan on 9/07/2015.
 */
public class CalculateResultError implements ICalculatorResult {
    public String ErrorMessage;
    public CalculateResultError(String error){
        ErrorMessage = error;

    }
    public String getString(){
        return ErrorMessage;
    }
}
